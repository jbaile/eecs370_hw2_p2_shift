#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

int main() {

    int64_t s = 0xffffffffffffffff;
    printf("int64_t before:\t\t%016lx\n", s);
    s = s >> 4;
    printf("int64_t after:\t\t%016lx\n", s);

    uint64_t u = 0xffffffffffffffff;
    printf("uint64_t before:\t%016lx\n", u);
    u = u >> 4;
    printf("uint64_t after:\t\t%016lx\n", u);

    return 0;
}
